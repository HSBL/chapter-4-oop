// console.log("Hello World!");

const hitung = function diskon(x) {
  let hasil = (x * 30) / 100;
  return hasil;
};

const hitungArrow = (x) => (x * 30) / 100;

const penambahan = (a, b) => a + b;

function sayHi() {
  return "Hallo";
}

// console.log(sayHi());

class Mobil {
  constructor(merk, warna) {
    this.merk = merk;
    this.warna = warna;
  }


  jumlahRoda = 4;

  gas(){
    console.log("BRRRMMMMM!!!!!!")
  }
}

console.log(Mobil.jumlahRoda);

const Mobilio = new Mobil("Honda", "Merah");
const Jazz = new Mobil("Honda", "Merah");
const City = new Mobil("Honda", "Merah");

class MobilModivan extends Mobil {
  constructor(merk, warna, budget){
    super(merk, warna)
    this.budget = budget
  }

  gas(string){
    super.gas()
    console.log(string)
  }

}

let mobilku = new MobilModivan("Toyota", "biru", 5000)

mobilku.gas("gas polll!!")


console.log(mobilku.jumlahRoda)

Mobilio.gas()
// City.gas()

const has = {
  umur : 18,
  tinggi: 170,
  alamat: "Jakarta"
}